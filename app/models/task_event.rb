# frozen_string_literal: true

# Model that represents the Task Event
class TaskEvent < ApplicationRecord
  belongs_to :task
end
