# frozen_string_literal: true

# Model that represents the Task
class Task < ApplicationRecord
  has_many :task_events
end
