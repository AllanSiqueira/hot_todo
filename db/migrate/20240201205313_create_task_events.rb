# frozen_string_literal: true

class CreateTaskEvents < ActiveRecord::Migration[7.1]
  def change
    create_table :task_events do |t|
      t.integer :type
      t.belongs_to :task, null: false, foreign_key: true
      t.integer :from
      t.integer :status

      t.timestamps
    end
  end
end
